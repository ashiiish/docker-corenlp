FROM java:8

RUN mkdir /corenlp
WORKDIR /corenlp
RUN wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-02-27.zip; \
        unzip stanford-corenlp-full-2018-02-27.zip
WORKDIR /corenlp/stanford-corenlp-full-2018-02-27
RUN wget http://nlp.stanford.edu/software/stanford-english-corenlp-2018-02-27-models.jar
RUN wget http://nlp.stanford.edu/software/stanford-english-kbp-corenlp-2018-02-27-models.jar

COPY entrypoint.sh entrypoint.sh
EXPOSE 9000
ENTRYPOINT ["/corenlp/stanford-corenlp-full-2018-02-27/entrypoint.sh"]
