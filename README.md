# corenlp docker image

A set of docker images for [standford corenlp](https://stanfordnlp.github.io/CoreNLP/)

# Usage

`docker run -p 9000:9000 registry.gitlab.com/ashneo76/docker-corenlp:3.9.1-0.0.1`